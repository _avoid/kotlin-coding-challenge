package com.example.tsetcodingchallange.rest

import com.example.tsetcodingchallange.business.VersionService
import com.example.tsetcodingchallange.domain.ServiceState
import com.example.tsetcodingchallange.domain.SystemState
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException

@RestController
class ReleaseManagerController(var versionService: VersionService) {

    @PostMapping("/deploy")
    fun deploy(@RequestBody service: ServiceState): Int {
        println("Received: $service")
        versionService.deployService(service)
        return versionService.currentSystemState().systemVersion
    }

    @GetMapping("/services")
    fun services(@RequestParam systemVersion: Int): Array<ServiceState> {
        val result = versionService.findSystemStateForVersion(systemVersion)
        return result?.serviceVersions ?: throw ResponseStatusException(
            HttpStatus.NOT_FOUND, "Version not found"
        )
    }

    // Not part of the API specification, but great for Debugging
    @GetMapping("/systemState")
    fun services(): SystemState {
        return versionService.currentSystemState()
    }

}
