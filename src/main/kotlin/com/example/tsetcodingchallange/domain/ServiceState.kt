package com.example.tsetcodingchallange.domain

data class ServiceState(var name: String, var version: Int)
