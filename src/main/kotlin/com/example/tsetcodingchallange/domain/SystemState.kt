package com.example.tsetcodingchallange.domain

data class SystemState(var systemVersion: Int, var serviceVersions: Array<ServiceState>) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SystemState

        if (systemVersion != other.systemVersion) return false
        if (!serviceVersions.contentEquals(other.serviceVersions)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = systemVersion
        result = 31 * result + serviceVersions.contentHashCode()
        return result
    }
}
