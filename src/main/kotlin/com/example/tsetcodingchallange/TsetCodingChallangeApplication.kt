package com.example.tsetcodingchallange

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TsetCodingChallangeApplication

fun main(args: Array<String>) {
    runApplication<TsetCodingChallangeApplication>(*args)
}
