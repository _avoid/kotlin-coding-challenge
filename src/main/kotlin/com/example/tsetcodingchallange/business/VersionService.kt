package com.example.tsetcodingchallange.business

import com.example.tsetcodingchallange.domain.ServiceState
import com.example.tsetcodingchallange.domain.SystemState
import org.springframework.stereotype.Service

@Service
class VersionService {

    var systemVersionHistory: MutableList<SystemState> = mutableListOf(
        SystemState(
            systemVersion = 0,
            serviceVersions = emptyArray()
        )
    )

    @Synchronized
    fun deployService(service: ServiceState) {
        val currentSystemState = currentSystemState()
        val currentServiceState = currentSystemState.serviceVersions.find { it.name == service.name }

        if (serviceHasChanged(currentServiceState, service)) {
            if (currentServiceState == null) {
                createNewService(currentSystemState, service)
            }
            else {
                updateExistingService(currentSystemState, service)
            }

        }
        println(systemVersionHistory)
    }

    private fun createNewService(
        currentSystemState: SystemState,
        service: ServiceState
    ) {
        systemVersionHistory.add(
            SystemState(
                currentSystemState.systemVersion + 1,
                currentSystemState.serviceVersions.plus(service)
            )
        )
    }

    private fun updateExistingService(
        currentSystemState: SystemState,
        service: ServiceState
    ) {
        val serviceVersions = currentSystemState.serviceVersions.map { it.copy() }.toTypedArray()
        val serviceToUpdate = serviceVersions.find { it.name == service.name }
        serviceToUpdate?.version = service.version
        systemVersionHistory.add(
            SystemState(
                currentSystemState.systemVersion + 1,
                serviceVersions
            )
        )
    }


    fun findSystemStateForVersion(systemVersion: Int): SystemState? {
        return systemVersionHistory.find { it.systemVersion == systemVersion }
    }

    private fun serviceHasChanged(previous: ServiceState?, current: ServiceState): Boolean {
        return if (previous == null) {
            true
        } else {
            previous.version != current.version
        }
    }

    fun currentSystemState(): SystemState {
        return systemVersionHistory.last()
    }
}
