package com.example.tsetcodingchallange.business

import com.example.tsetcodingchallange.domain.ServiceState
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class VersionServiceTest {

    private var versionService = VersionService()

    private fun assertVersionToBe(expectedVersion: Int) {
        Assertions.assertEquals(versionService.currentSystemState().systemVersion, expectedVersion)
    }

    @Test
    fun `initial system version should be 0`() {
        assertVersionToBe(0)
    }


    @Test
    fun `test interaction from specification`() {
        versionService.deployService(ServiceState("Service A", 1))
        assertVersionToBe(1)

        versionService.deployService(ServiceState("Service B", 1))
        assertVersionToBe(2)

        versionService.deployService(ServiceState("Service A", 2))
        assertVersionToBe(3)

        versionService.deployService(ServiceState("Service B", 1))
        assertVersionToBe(3)


        // Requirements for systemVersion 2
        val stateVersion2 = versionService.findSystemStateForVersion(2)
        Assertions.assertEquals(stateVersion2?.systemVersion, 2)
        val serviceA = stateVersion2?.serviceVersions?.find { it.name == "Service A" }
        Assertions.assertEquals(serviceA?.version, 1)

        val serviceB = stateVersion2?.serviceVersions?.find { it.name == "Service B" }
        Assertions.assertEquals(serviceB?.version, 1)


        // Requirements for systemVersion 3
        val stateVersion3 = versionService.findSystemStateForVersion(3)
        Assertions.assertEquals(stateVersion3?.systemVersion, 3)
        val serviceA3 = stateVersion3?.serviceVersions?.find { it.name == "Service A" }
        Assertions.assertEquals(serviceA3?.version, 2)

        val serviceB3 = stateVersion3?.serviceVersions?.find { it.name == "Service B" }
        Assertions.assertEquals(serviceB3?.version, 1)

    }
}
