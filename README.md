# Coding challenge - for X

See `Challenge.md` for task and planned approach


## How to run

* Simple Spring Boot application created with sprint initializer (IntelliJ)
* Standard gradle commands apply 
* Developed with JDK17


## How to test
* See unit test
* or import `postman_collection.json` into Postman (local url and port might differ)
